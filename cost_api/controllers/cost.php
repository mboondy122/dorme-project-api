<?php
class Cost{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function isOddNum($num){
        try {
            if($num % 2 == 0){
                responseJson(400, 'This Number is Even', null);
            }else{
                responseJson(200, 'This Number is Odd', null);
            }
        }catch(Exception $err){ 
            responseJson(500, $err->getMessage(), null);
        }
    }
    function uuidv4(){
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
    function addCost($inpData){
        $cost_id = $this->uuidv4();
        $query = "INSERT INTO `billcost` (`cost_id`,`room`,`elc_cost`,`water_cost`,`room_cost`,`date_cost`,`elc_unit`,`total`,`status`,`month_cost`) VALUES (:cost_id,:room,:elc_cost,:water_cost,:room_cost,NOW(),:elc_unit,:total,'ค้างชำระ',:month_cost);";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":cost_id",$cost_id,PDO::PARAM_STR);
            $stmt1->bindParam(":room",$inpData['room'],PDO::PARAM_STR);
            $stmt1->bindParam(":elc_cost",$inpData['elc_cost'],PDO::PARAM_STR);
            $stmt1->bindParam(":water_cost",$inpData['water_cost'],PDO::PARAM_STR);
            $stmt1->bindParam(":room_cost",$inpData['room_cost'],PDO::PARAM_STR);
            $stmt1->bindParam(":elc_unit",$inpData['elc_unit'],PDO::PARAM_STR);
            $stmt1->bindParam(":total",$inpData['total'],PDO::PARAM_STR);
            $stmt1->bindParam(":month_cost",$inpData['month_cost'],PDO::PARAM_STR);
            $stmt1->execute();
            
            responseJson(200, 'add billcost successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);        
        }
    }
    function deleteCost($cost_id){
        $query2 = "DELETE FROM `billcost` WHERE `cost_id`=:cost_id;";
        try{
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":cost_id",$cost_id,PDO::PARAM_STR);
            $stmt2->execute();
            
            responseJson(200, 'billcost deleted successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);       
        }
    }
    function editStatusCost($inpData){
        $query2 = "UPDATE `billcost` SET `status`=:status,`date_modify`=NOW() WHERE `cost_id`=:cost_id;";
        try{
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":status",$inpData['status'],PDO::PARAM_STR);
            $stmt2->bindParam(":cost_id",$inpData['cost_id'],PDO::PARAM_STR);
            $stmt2->execute();
            
            responseJson(200, 'billcost status updated successfully', null);
        
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);    
        }
    }
    function getListRoomCost($room){
        $query = "SELECT * FROM `billcost` WHERE `room`=:room ORDER BY `date_cost` DESC;";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":room",$room,PDO::PARAM_STR);
            $stmt1->execute();
            $data = array();
            
            while ($row=$stmt1->fetch()){
                $el = array(
                    "cost_id"       =>$row["cost_id"],
                    "date_cost"     =>$row['date_cost'],
                    "room"          =>$row['room'],
                    "elc_cost"      =>$row['elc_cost'],
                    "water_cost"    =>$row['water_cost'],
                    "room_cost"     =>$row['room_cost'],
                    "elc_unit"      =>$row['elc_unit'],
                    "total"         =>$row['total'],
                    "month_cost"    =>$row['month_cost'],
                    "status"        =>$row['status']
                );
                array_push($data,$el);
            }

            responseJson(200, 'get list cost successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);   
        } 
    }
    function getDetailCost($cost_id){
        $query = "SELECT * FROM `billcost` WHERE `cost_id`=:cost_id ORDER BY `date_cost` DESC;";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":cost_id",$cost_id,PDO::PARAM_STR);
            $stmt1->execute();
            $data = array();
            
            while ($row=$stmt1->fetch()){
                $el = array(
                    "cost_id"       =>$row["cost_id"],
                    "date_cost"     =>$row['date_cost'],
                    "room"          =>$row['room'],
                    "elc_cost"      =>$row['elc_cost'],
                    "water_cost"    =>$row['water_cost'],
                    "room_cost"     =>$row['room_cost'],
                    "elc_unit"      =>$row['elc_unit'],
                    "total"         =>$row['total'],
                    "status"        =>$row['status']
                );
                array_push($data,$el);
            }
            responseJson(200, 'get list cost successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);   
        }
    }
    function addReceiptCost($inpData){
        $query = "INSERT INTO `receipt_cost` (`cost_id`,`account_id`,`receipt_img`,`receipt_date`,`room`,`receipt_month`) VALUES (:cost_id,:account_id,:receipt_img,NOW(),:room,:receipt_month);";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":cost_id",$inpData['cost_id'],PDO::PARAM_STR);
            $stmt1->bindParam(":account_id",$inpData['account_id'],PDO::PARAM_STR);
            $stmt1->bindParam(":receipt_img",$inpData['receipt_img'],PDO::PARAM_STR);
            $stmt1->bindParam(":room",$inpData['room'],PDO::PARAM_STR);
            $stmt1->bindParam(":receipt_month",$inpData['receipt_month'],PDO::PARAM_STR);
            $stmt1->execute();

            responseJson(200, 'add reciptcost successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);   
        }
    }
    function getReceiptCost(){
        $query = "SELECT * FROM `receipt_cost`";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":receipt_id",$receipt_id,PDO::PARAM_STR);
            $stmt1->execute();
            
            $data = array();
            while ($row=$stmt1->fetch()){
                $el = array(
                    "receipt_id"        => $row["receipt_id"],
                    "cost_id"           => $row["cost_id"],
                    "account_id"        => $row["account_id"],
                    "receipt_date"      => $row["receipt_date"],
                    "room"              => $row["room"],
                    "receipt_month"     => $row["receipt_month"]
                );
                array_push($data,$el);
            }
            
            responseJson(200, 'get reciptcost successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);  
        }
    }
    function getInfoReceiptCost($receipt_id){
        $query = "SELECT * FROM `receipt_cost` WHERE `receipt_id`=:receipt_id";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":receipt_id",$receipt_id,PDO::PARAM_STR);
            $stmt1->execute();
            $row=$stmt1->fetch();
            
            $data = array(
                "receipt_img"     =>$row["receipt_img"]
            );
            
            responseJson(200, 'get reciptcost successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);  
        }
    }
    function selectMonthCost($room,$month_cost){
        $query = "SELECT * FROM `billcost` WHERE `room`=:room AND `month_cost`=:month_cost";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":room",$room,PDO::PARAM_STR);
            $stmt1->bindParam(":month_cost",$month_cost,PDO::PARAM_STR);
            $stmt1->execute();

            $data = array();
            while ($row=$stmt1->fetch()){
                $el = array(
                    "cost_id"       =>$row["cost_id"],
                    "room"          =>$row['room'],
                    "total"         =>$row['total'],
                    "month_cost"    =>$row['month_cost'],
                    "status"        =>$row['status']
                );
                array_push($data,$el);
            }

            responseJson(200, 'get bill cost successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function addBanking($inpData){
        $query = "INSERT INTO `banking` (`bank`,`number_banking`,`name_banking`,`banking_id`) VALUES (:bank,:number_banking,:name_banking,:banking_id);";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":bank",$inpData['bank'],PDO::PARAM_STR);
            $stmt1->bindParam(":number_banking",$inpData['number_banking'],PDO::PARAM_STR);
            $stmt1->bindParam(":name_banking",$inpData['name_banking'],PDO::PARAM_STR);
            $stmt1->bindParam(":banking_id",$inpData['banking_id'],PDO::PARAM_STR);
            $stmt1->execute();
            
            responseJson(200, 'add banking successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getBanking(){
        $query = "SELECT * FROM `banking`";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->execute();
            
            $data = array();
            while ($row=$stmt1->fetch()){
                $el = array(
                    "bank"               =>$row["bank"],
                    "number_banking"     =>$row["number_banking"],
                    "name_banking"       =>$row["name_banking"],
                    "banking_id"         =>$row["banking_id"]
                );
                array_push($data,$el);
            }

            responseJson(200, 'get banking successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function deleteBanking($banking_id){
        $query = "DELETE FROM `banking` WHERE `banking_id`=:banking_id;";
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(":banking_id",$banking_id,PDO::PARAM_STR);
            $stmt->execute();
            
            responseJson(200, 'banking deleted successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function deleteReceipt($receipt_id){
        $query = "DELETE FROM `receipt_cost` WHERE `receipt_id`=:receipt_id;";
        try{
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(":receipt_id",$receipt_id,PDO::PARAM_STR);
            $stmt->execute();

            responseJson(200, 'receipt deleted successfully', null);
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }

    function __destruct(){
        $this->conn = null;
    }
} 


?>