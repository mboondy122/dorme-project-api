<?php
include '../config/cors.php';
function responseJson($status, $msg = null, $data = null){
    $state = str_split(strval($status))[0];
    switch ($state) {
        case '2':
                http_response_code($status);
                echo json_encode(array("status"=>"success", "message"=> $msg, "data"=> $data));
            break;
        case '3':
                http_response_code($status);
                echo json_encode(array("status"=>"success", "message"=> $msg, "data"=> $data));
            break;
        case '4':
                http_response_code($status);
                echo json_encode(array("status"=>"fail", "message"=> $msg, "data"=> $data));
            break;
        case '5':
                http_response_code($status);
                echo json_encode(array("status"=>"error", "message"=> $msg, "data"=> $data));
            break;
        default:
            http_response_code(500);
            echo json_encode(array("status"=>"error", "message"=> $msg, "data"=> null));
    }
}



?>