<?php
header("Access-Control-allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
if($_SERVER['REQUEST_METHOD']=="GET"){
    require "../config/database.php";
    $db = new Database();
    $conn= $db->getConnectionWithStatus();
    if($conn['conn']==null){
        http_response_code(500);
        echo json_encode(array("status"=>"error", "message"=> $conn['msg'], "data"=> null));
    }else{
        http_response_code(200);
        echo json_encode(array("status"=>"ok", "message"=> "Database Connected Successfully", "data"=> null));
    }
}else{
    http_response_code(404);
    echo json_encode(array("status"=>"fail", "message"=> "path or method not found", "data"=> null));
}
?>