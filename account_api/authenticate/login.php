<?php
if($_SERVER['REQUEST_METHOD']=="POST"){
    $reqJson = json_decode(file_get_contents('php://input'), true);
    require "../controllers/authenticate.php";
    $auth = new Authenticate();
    $resData= $auth->login($reqJson); 
}else{
    include '../config/missing-method.php';
}
?>