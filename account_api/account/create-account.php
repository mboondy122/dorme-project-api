<?php
if($_SERVER['REQUEST_METHOD']=="POST"){
    $reqJson = json_decode(file_get_contents('php://input'), true);
    require "../controllers/account.php";
    $account = new Account();
    $resData= $account->createAccount($reqJson); 
}else{
    include '../config/missing-method.php';
}
?>