<?php
class Authenticate{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function generateToken($username, $password) {
        $salt = $_ENV['SALT'];
        $token = sha1($username . $salt . $password);
        return $token;
    }  
    function login($inpData){
        $query = "SELECT * FROM `account` WHERE `username`=:username;";
        $tokenNew = $this->generateToken($inpData['username'],$inpData['password']);
        $query2 = "SELECT * FROM `user` WHERE `account_id`=:account_id;";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":username",$inpData['username'],PDO::PARAM_STR);
            $stmt1->execute();
            $row = $stmt1->fetch();
            if ($row < 1) {
                responseJson(401, 'username not fount', null);
            } else {
                $countFail = $row["fail"];
                $timeOut = $row['blacklistTimeout'];
                $timeNow = strtotime("now");
                if($row['blacklistTimeout'] == null){
                    if ($countFail >= 5) {
                        $sql = "UPDATE `account` SET `blacklistTimeout`=:curr WHERE `account_id`=:account_id;";
                        $stmt = $this->conn->prepare($sql);
                        $stmt->bindParam(":account_id", $row['account_id'], PDO::PARAM_STR);
                        $blacklistTimeout = $timeNow + 86400;
                        $stmt->bindParam(":curr", $blacklistTimeout, PDO::PARAM_STR);
                        $stmt->execute();
                        responseJson(403, 'login fail more then 5 times', intval($blacklistTimeout));
                    } else {
                        if ($row["token"] != $tokenNew) {
                            if ($countFail >= 5) {
                                $countFail = 0;
                            }
                            $checkFail = $countFail + 1;
                            $sql = "UPDATE `account` SET `fail`=:checkFail WHERE `account_id`=:account_id;";
                            $stmt = $this->conn->prepare($sql);
                            $stmt->bindParam(":checkFail", $checkFail, PDO::PARAM_STR);
                            $stmt->bindParam(":account_id", $row['account_id'], PDO::PARAM_STR);
                            $stmt->execute();

                            responseJson(403, 'password incorrect', null);
                        } else {
                            $stmt2 = $this->conn->prepare($query2);
                            $stmt2->bindParam(":account_id", $row["account_id"], PDO::PARAM_STR);
                            $stmt2->execute();
                            $row2 = $stmt2->fetch();

                            $sql = "UPDATE `account` SET `fail`='0' WHERE `account_id`=:account_id;";
                            $stmt = $this->conn->prepare($sql);
                            $stmt->bindParam(":account_id", $row['account_id'], PDO::PARAM_STR);
                            $stmt->execute();

                            responseJson(201, 'login successfully', $row2);
                        }
                    }
                }
                elseif ($timeNow > $timeOut) {
                    $sql = "UPDATE `account` SET `blacklistTimeout` = null,`fail`=0 WHERE `account_id`=:account_id;";
                    $stmt = $this->conn->prepare($sql);
                    $stmt->bindParam(":account_id", $row['account_id'], PDO::PARAM_STR);
                    $stmt->execute();
                    
                    if ($row["token"] != $tokenNew) {
                        if ($countFail >= 5) {
                            $countFail = 0;
                        }
                        $checkFail = $countFail + 1;
                        $sql = "UPDATE `account` SET `fail`=:checkFail WHERE `account_id`=:account_id;";
                        $stmt = $this->conn->prepare($sql);
                        $stmt->bindParam(":checkFail", $checkFail, PDO::PARAM_STR);
                        $stmt->bindParam(":account_id", $row['account_id'], PDO::PARAM_STR);
                        $stmt->execute();

                        responseJson(403, 'password incorrect', null);
                    } else {
                        $stmt2 = $this->conn->prepare($query2);
                        $stmt2->bindParam(":account_id", $row["account_id"], PDO::PARAM_STR);
                        $stmt2->execute();
                        $row2 = $stmt2->fetch();

                        $sql = "UPDATE `account` SET `fail`='0' WHERE `account_id`=:account_id;";
                        $stmt = $this->conn->prepare($sql);
                        $stmt->bindParam(":account_id", $row['account_id'], PDO::PARAM_STR);
                        $stmt->execute();

                        responseJson(201, 'login successfully', $row2);
                    }
                }else{
                    responseJson(403, 'login fail more then 5 time', $timeNow);
                }
            }
        }catch(Exception $e){
            responseJson(500, $e->getMessage(), null);
        }
    }
    function __destruct(){
        $this->conn = null;
    }
} 
?>