<?php
class Account{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function generateToken($username, $password) {
        $salt = $_ENV['SALT'];
        $token = sha1($username . $salt . $password);
        return $token;
    }  
    function createAccount($inpData) {
        $account_id = uniqid();
        $token = $this->generateToken($inpData['username'],$inpData['password']);
        $query = "INSERT INTO `account` (`account_id`,`username`,`token`)VALUES (:account_id,:username,:token);";
        $query2 = "INSERT INTO `user` (`account_id`,`fname`,`lname`,`tel`,`room`,`email`,`status`,`age`)VALUES (:account_id,:fname,:lname,:tel,:room,:email,'มีผู้เช่า',:age);";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":account_id",$account_id,PDO::PARAM_STR);
            $stmt1->bindParam(":username",$inpData['username'],PDO::PARAM_STR);
            $stmt1->bindParam(":token",$token,PDO::PARAM_STR);
            $stmt1->execute();
            
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":account_id",$account_id,PDO::PARAM_STR);
            $stmt2->bindParam(":fname",$inpData['fname'],PDO::PARAM_STR);
            $stmt2->bindParam(":lname",$inpData['lname'],PDO::PARAM_STR);
            $stmt2->bindParam(":tel",$inpData['tel'],PDO::PARAM_STR);
            $stmt2->bindParam(":room",$inpData['room'],PDO::PARAM_STR);
            $stmt2->bindParam(":email",$inpData['email'],PDO::PARAM_STR);
            $stmt2->bindParam(":age",$inpData['age'],PDO::PARAM_STR);
            $stmt2->execute();

            responseJson(201, 'account created successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    
    }
    function deleteAccount($account_id){
        $query2 = "DELETE FROM `user` WHERE `account_id`=:account_id;";
        $query3 = "DELETE FROM `account` WHERE `account_id`=:account_id;";
        try{
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":account_id",$account_id,PDO::PARAM_STR);
            $stmt2->execute();

            $stmt3 = $this->conn->prepare($query3);
            $stmt3->bindParam(":account_id",$account_id,PDO::PARAM_STR);
            $stmt3->execute();
            
            responseJson(200, 'account deleted successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function editProfile($inpData){
        $query4 = "UPDATE `user` SET `fname`=:fname, `lname`=:lname, `tel`=:tel, `email`=:email, `age`=:age WHERE `account_id`=:account_id;";
        try{
            $stmt4 = $this->conn->prepare($query4);
            $stmt4->bindParam(":fname",$inpData['fname'],PDO::PARAM_STR);
            $stmt4->bindParam(":lname",$inpData['lname'],PDO::PARAM_STR);
            $stmt4->bindParam(":tel",$inpData['tel'],PDO::PARAM_STR);
            $stmt4->bindParam(":email",$inpData['email'],PDO::PARAM_STR);
            $stmt4->bindParam(":age",$inpData['age'],PDO::PARAM_STR);
            $stmt4->bindParam(":account_id",$inpData['account_id'],PDO::PARAM_STR);
            $stmt4->execute();
            
            responseJson(200, 'profile updated successfully', null);

        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function editPassword($inpData){
        $tokenNew = $this->generateToken($inpData['username'],$inpData['passwordNew']);
        $query2 = "UPDATE `account` SET `token`=:token WHERE `account_id`=:account_id;";
        try{
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":account_id",$inpData['account_id'],PDO::PARAM_STR);
            $stmt2->bindParam(":token",$tokenNew,PDO::PARAM_STR);
            $stmt2->execute();
            
            responseJson(200, 'password updated successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getListRoomProfile($account_id){
        $query = "SELECT * FROM `user` WHERE `account_id`=:account_id;";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":account_id",$account_id,PDO::PARAM_STR);
            $stmt1->execute();
            $data = array();
            while ($row=$stmt1->fetch()){
                $el = array(
                    "fname" =>$row['fname'],
                    "lanme"=>$row['lname'],
                    "tel"=>$row['tel'],
                    "email"=>$row['email'],
                    "room"=>$row['room'],
                    "age"=>$row['age']
                );
                array_push($data,$el);
            }
            responseJson(200, 'get list profile successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getListAllProfile(){
        $query = "SELECT * FROM `user` ORDER BY `room` ASC";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->execute();
            $data = array();
            while ($row=$stmt1->fetch()){
                $el = array(
                    "status"=>$row['status'],
                    "room"=>$row['room'],
                    "account_id"=>$row['account_id']
                );
                array_push($data,$el);
            }
            responseJson(200, 'get list profile successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function resetLogin($account_id){
        $sql = "UPDATE `account` SET `blacklistTimeout` = null,`fail`=0 WHERE `account_id`=:account_id;";
        try{
            $stmt1 = $this->conn->prepare($sql);
            $stmt1->bindParam(":account_id", $account_id, PDO::PARAM_STR);
            $stmt1->execute();

            responseJson(201, 'reset login time', null);
        } catch (PDOException $err) {
            responseJson(500, $err->getMessage(), null);
        }
    }

    function __destruct(){
        $this->conn = null;
    }
} 
?>