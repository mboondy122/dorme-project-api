<?php
class Announce{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }

    function isOddNum($num){
        try {
            if($num % 2 == 0){
                responseJson(400, 'This Number is Even', null);
            }else{
                responseJson(200, 'This Number is Odd', null);
            }
        }catch(Exception $err){ 
            responseJson(500, $err->getMessage(), null);
        }
    }
    function addAnnounce($inpData){
        $annouce_id = uniqid();
        $query = "INSERT INTO `annouce` (`announce_id`,`date`,`topic`,`description`)VALUES (:announce_id,NOW(),:topic,:description);";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":announce_id",$annouce_id,PDO::PARAM_STR);
            $stmt1->bindParam(":topic",$inpData['topic'],PDO::PARAM_STR);
            $stmt1->bindParam(":description",$inpData['description'],PDO::PARAM_STR);
            $stmt1->execute();

            responseJson(200, 'add annouce successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getListAnnounce(){
        $query = "SELECT * FROM `annouce` ORDER BY `date` DESC;";

        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->execute();
            $data = array();
                while ($row=$stmt1->fetch()){
                $el = array(
                    "announce_id" => $row["announce_id"],
                    "date" => $row['date'],
                    "topic"=>$row['topic'],
                    "description"=>$row['description']
                );
                array_push($data,$el);
            }
            responseJson(200, 'get list anal successfully', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function deleteAnnounce($announce_id){
        $query2 = "DELETE FROM `annouce` WHERE `announce_id`=:announce_id;";
        try{
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":announce_id",$announce_id,PDO::PARAM_STR);
            $stmt2->execute();
            responseJson(200, 'delete anal successfully', null);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getListTopicAnnounce ($announce_id){
            $query = "SELECT * FROM `annouce` WHERE `announce_id`=:announce_id;";
            try{
                $stmt1 = $this->conn->prepare($query);
                $stmt1->bindParam(":announce_id",$announce_id,PDO::PARAM_STR);
                $stmt1->execute();
                $data = array();
                 while ($row=$stmt1->fetch()){
                    $el = array(
                        "date" => $row['date'],
                        "topic"=>$row['topic'],
                        "description"=>$row['description']
                    );
                    array_push($data,$el);
                }
                responseJson(200, 'get info anal successfully', $data);
            }catch(PDOException $err){
                responseJson(500, $err->getMessage(), null);
            }
        }

    function __destruct(){
        $this->conn = null;
    }
} 
?>