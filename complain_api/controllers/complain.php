<?php
class Complain{
    private $conn = null;
    function __construct(){
        include '../config/response.php';
        require "../config/database.php";
        $db = new Database();
        $this->conn = $db->getConnection();
    }
    function uuidv4() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    function isOddNum($num){
        try {
            if($num % 2 == 0){
                responseJson(400, 'This Number is Even', null);
            }else{
                responseJson(200, 'This Number is Odd', null);
            }
        }catch(Exception $err){ 
            responseJson(500, $err->getMessage(), null);
        }
    }
    function addComplain($inpData){           
        $comp_id = $this->uuidv4();
        $query = "INSERT INTO `complain` (`complain_id`,`topic`,`description`,`account_id`,`name`,`tel`,`room`,`date`,`status`) VALUES (:complain_id,:topic,:description,:account_id,:name,:tel,:room,NOW(),'ยังไม่ได้รับเรื่อง');";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":complain_id",$comp_id,PDO::PARAM_STR);
            $stmt1->bindParam(":topic",$inpData['topic'],PDO::PARAM_STR);
            $stmt1->bindParam(":description",$inpData['description'],PDO::PARAM_STR);
            $stmt1->bindParam(":account_id",$inpData['account_id'],PDO::PARAM_STR);
            $stmt1->bindParam(":name",$inpData['name'],PDO::PARAM_STR);
            $stmt1->bindParam(":tel",$inpData['tel'],PDO::PARAM_STR);
            $stmt1->bindParam(":room",$inpData['room'],PDO::PARAM_STR);
            $stmt1->execute();

            responseJson(200, 'Add complain successfully', null);

        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getListComplain (){
        $query = "SELECT * FROM `complain` ORDER BY `date` DESC;";
        
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->execute();
            $data = array();
            
            while ($row=$stmt1->fetch()){
                $el = array(
                    "description" =>$row['description'],
                    "date" =>$row['date'],
                    "topic"=>$row['topic'],
                    "status"=>$row['status'],
                    "room"=>$row['room']
                    
                );
                array_push($data,$el);
            }
            responseJson(200, 'get list complain', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function getListRoomComplain ($room){
        $query = "SELECT * FROM `complain` WHERE `room`=:room ORDER BY `date` DESC;";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":room",$room,PDO::PARAM_STR);
            $stmt1->execute();
            $data = array();
            
            while ($row=$stmt1->fetch()){
                $el = array(
                    "complain_id" =>$row['complain_id'],
                    "description" =>$row['description'],
                    "date" =>explode(" ",$row['date'])[0],
                    "topic"=>$row['topic'],
                    "status"=>$row['status'],
                    "room"=>$row['room']
                    
                );
                array_push($data,$el);
            }
            responseJson(200, 'get list complain', $data);
            
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function editStatusComplain ($inpData){
        $query2 = "UPDATE `complain` SET `status`=:status WHERE `date`=:date;";
        try{
            $stmt2 = $this->conn->prepare($query2);
            $stmt2->bindParam(":status",$inpData['status'],PDO::PARAM_STR);
            $stmt2->bindParam(":date",$inpData['date'],PDO::PARAM_STR);
            $stmt2->execute();

            responseJson(200, 'update status successfully', null);
        
        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }
    function deleteComplain($complain_id){
        $query = "DELETE FROM `complain` WHERE `complain_id`=:complain_id;";
        try{
            $stmt1 = $this->conn->prepare($query);
            $stmt1->bindParam(":complain_id",$complain_id,PDO::PARAM_STR);
            $stmt1->execute();

            responseJson(200, 'complain deleted successfully', null);

        }catch(PDOException $err){
            responseJson(500, $err->getMessage(), null);
        }
    }

    function __destruct(){
        $this->conn = null;
    }
} 


?>